class Sim {
	int a=9;
	void getter(int b) {
		System.out.printf(" a value %d\n",a);
		this.a =b;
		System.out.printf(" a value %d\n",a);
	}
	Sim() {
		this(12);
		System.out.printf("value at a %d\n",a);

	}
	Sim(int a) {
		System.out.printf("Cons with single args %d\n",a);
	}
	public static void main(String[] args) {
		Sim s = new Sim();
		s.getter(12);
		
	}
}