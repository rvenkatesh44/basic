abstract class abbs
{
	abbs()
	{
		System.out.println("constuctor\n");
	}
	
	abstract void simple();
	
	void sample()
	{
		System.out.println("sample method");
	}

}

class abst extends abbs
{
	void simple()
	{
		System.out.println("simple mmm method");
	}

	public static void main(String argsp[])
	{
		abst a = new abst();
		a.simple();
		a.sample();
	}
}